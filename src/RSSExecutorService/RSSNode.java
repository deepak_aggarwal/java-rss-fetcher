/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RSSExecutorService;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;



/**
 *
 * @author Deepak
 */
public class RSSNode<B,C> {
    B to_be_send;
    CountDownLatch latch;
    ConcurrentLinkedQueue result_queue;
    public RSSNode(B to_be_send, CountDownLatch latch, ConcurrentLinkedQueue result_queue)
    {
        this.to_be_send=to_be_send;
        this.latch=latch;
        this.result_queue= result_queue;
    }
    public B getData()
    {
        return to_be_send;
    }
    public CountDownLatch getLatch()
    {
        return latch;
    }
    public Boolean putResult ( C data)
    {
        return result_queue.add(data);
    }
            
}
