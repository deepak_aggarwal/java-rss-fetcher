/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RSSExecutorService;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Deepak
 */
public class RSSExecutorService <A,B,C>{
    ExecutorService threadpool= null;
    ArrayBlockingQueue queue;
    ConcurrentLinkedQueue bridge_queue;
    ConcurrentLinkedQueue  result_queue;
    CountDownLatch latch=null;
    
    public RSSExecutorService(String class_name,int no_of_threads) throws ClassNotFoundException, IllegalAccessException, 
            InstantiationException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
    {
        threadpool= Executors.newFixedThreadPool(no_of_threads);
        bridge_queue= new ConcurrentLinkedQueue<RSSNode>();   
        result_queue= new ConcurrentLinkedQueue<C>();
        queue = new ArrayBlockingQueue<A>(no_of_threads);
        //Reflection 
        Class temp=Class.forName(class_name);
        Class[] argument = new Class[]{ArrayBlockingQueue.class, ConcurrentLinkedQueue.class};
        Constructor[] temp_constructor = temp.getConstructors();
        Object[] objects = new Object[]{queue,bridge_queue}; 
        for(int i=1;i<=no_of_threads;i++)
        {
            A ob=null;
            ob=(A) temp_constructor[0].newInstance(objects);
            if(ob!=null)
            try {
                queue.put(ob);      // Right now I am taking first constructor by default
            } catch (InterruptedException ex) {
                Logger.getLogger(RSSExecutorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public Boolean execute(B to_be_send)
    {
        bridge_queue.add(new RSSNode(to_be_send,latch,result_queue));
        try {
            threadpool.execute((Runnable) queue.take());
        } catch (InterruptedException ex) {
            Logger.getLogger(RSSExecutorService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    public Boolean registerLatch(CountDownLatch latch)
    {
        this.latch= latch;
        if(latch!=null)
            return true;
        else
            return false;
    }
    public Boolean shutdown()
    {
        try {
            this.latch.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(RSSExecutorService.class.getName()).log(Level.SEVERE, null, ex);
        }
        threadpool.shutdown();
        if(threadpool.isTerminated())
        return true;
        else
            return false;
    }
    public ConcurrentLinkedQueue<C> getResult()
    {
       return result_queue;
    }
}

