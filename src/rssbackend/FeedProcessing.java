/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rssbackend;

import RSSExecutorService.*;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Deepak
 */
public class FeedProcessing implements Runnable {
    Document doc= null;
    SyndEntry item= null;
    ConcurrentLinkedQueue bridge_reference;
    ArrayBlockingQueue queue_reference;
    
    // Image Fetcher 
    RSSExecutorService thread_pool=null;
    static CountDownLatch latch= null;
    ConcurrentLinkedQueue<Element> processor_imagefetcher_bridge= null;
    
    // Constructor
    public FeedProcessing(ArrayBlockingQueue temp,ConcurrentLinkedQueue bridge)
    {
        this.bridge_reference=bridge;
        this.queue_reference = temp;
        try {
            //Image Fetcher
            thread_pool= new RSSExecutorService<ImageFetcher,Element,ImageNode>("rssbackend.ImageFetcher",2);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
        processor_imagefetcher_bridge = new ConcurrentLinkedQueue<Element>();   // Initializing Communicating queue between FeedProcessor and ImageFetcher
        
        //  Filling up processor_imagefetcher_queue 
    }
    
    public void saveImages() throws IOException
    {
       ConcurrentLinkedQueue results = thread_pool.getResult();
       ImageNode image ;
       
        if(results.isEmpty())             // If there is no image then return
            return ;
       int max_x=0;
       int max_y=0;
       BufferedImage image_iterate;
       for(Iterator i=results.iterator(); i.hasNext();)
       {
           image = (ImageNode) i.next();
           image_iterate= image.actual_image;
           max_x+=image_iterate.getWidth();
           if(max_y<image_iterate.getHeight())
               max_y=image_iterate.getHeight();
       }
       BufferedImage new_image = new BufferedImage(max_x,max_y,BufferedImage.TYPE_3BYTE_BGR);
       Graphics g = new_image.getGraphics();
       int temp_x=0;
       for(Iterator i=results.iterator(); i.hasNext();)
       {
           image = (ImageNode) i.next();
           image_iterate= image.actual_image;
           g.drawImage(image_iterate, temp_x, 0, null);
           temp_x += image_iterate.getWidth();
       }
       results.clear();   // Cleaning and sanitizing queue
       File outputfile = new File("image_cache\\"+item.hashCode()+".png");
       ImageIO.write(new_image, "png", outputfile);
        
    }
    @Override
    public void run()
    {
        RSSNode item_instance= (RSSNode) bridge_reference.poll();
        if(item_instance==null)
        {
            try {
                queue_reference.put(this);  // Putting himself back to queue
                item_instance.getLatch().countDown();
            } catch (InterruptedException ex) {
                Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }
        try {
            //System.out.println(item.getTitle());
            item =(SyndEntry)item_instance.getData();
            
            // for atom feeds
            SyndContentImpl atom =(SyndContentImpl) item.getContents().get(0);
            if(atom!=null)
              doc=Jsoup.parse(atom.getValue());
            else  
              doc= Jsoup.parse(item.getDescription().getValue());  // for RSS feeds  Getting HTML Content of feed 
            
            
            Elements images = doc.getElementsByTag("img");
            latch = new CountDownLatch(images.size());
            thread_pool.registerLatch(latch);
            for(Element image: images)
            {
                thread_pool.execute(image);
            }
            latch.await(); // wait till all threads complete there work
            try {
                saveImages();
            } catch (IOException ex) {
                Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
            }
            queue_reference.put(this); // Putting himself back to queue
            item_instance.getLatch().countDown();
        } catch (InterruptedException ex) {
            Logger.getLogger(FeedProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    public Boolean shutdown() throws InterruptedException
    {
       latch.await();
        Boolean result = thread_pool.shutdown();
        return result;
    }
}
