/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rssbackend;

import java.awt.image.BufferedImage;
import org.jsoup.nodes.Element;

/**
 *
 * @author Deepak
 */

//Contains information of image nodes
public class ImageNode {
    Element image_element;
    BufferedImage actual_image;
    ImageNode(Element image_element,BufferedImage actual_image)
    {
       this.actual_image= actual_image;
       this.image_element = image_element;
    }
}
