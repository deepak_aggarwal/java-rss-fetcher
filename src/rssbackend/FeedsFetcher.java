/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rssbackend;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import RSSExecutorService.*;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author Deepak
 * 
 */
public class FeedsFetcher {
    URL url = null;
    XmlReader reader = null;
    SyndFeed feed = null;                      //for getting feeds
    static CountDownLatch latch= null;               // FIXME 
    RSSExecutorService thread_pool=null;
    ConcurrentLinkedQueue<SyndEntry> fetcher_processor_bridge;
    public FeedsFetcher()
    {
        
    }
    public void  fetchFeed(String url_to_fetched)
    {
        fetcher_processor_bridge = new ConcurrentLinkedQueue<SyndEntry>();
        
        // Initializing thread pool
        try {
            thread_pool = new RSSExecutorService<FeedProcessing,SyndEntry,ImageNode>("rssbackend.FeedProcessing",5);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Resolving URL
        try {
            url = new URL(url_to_fetched);             // Opening  Connection
        } catch (MalformedURLException ex) {
            Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            try {
                reader = new XmlReader(url);           //Parse Xml RSS feed Document 
            } catch (IOException ex) {
                Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                // All feeds are  stored in it.
                feed = new SyndFeedInput().build(reader); 
                latch = new CountDownLatch(feed.getEntries().size());
                thread_pool.registerLatch(latch);
                
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FeedException ex) {
                Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Feed Title: "+ feed.getAuthor());   
            for (Iterator i = feed.getEntries().iterator(); i.hasNext();)   // Iterating all feeds  
            {
                // First send required data
                thread_pool.execute((SyndEntry) i.next());
            }
        }finally {
            if (reader != null)
                try {
                reader.close();     // closing everything
            } catch (IOException ex) {
                Logger.getLogger(FeedsFetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
}   

