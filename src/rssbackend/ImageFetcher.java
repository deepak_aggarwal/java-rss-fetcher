/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rssbackend;

import RSSExecutorService.RSSNode;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jsoup.nodes.Element;

/**
 *
 * @author Deepak
 */
public class ImageFetcher implements Runnable{
    RSSNode image_node=null;
    
    // save Imge 
    URL url_instance= null;
    HttpURLConnection instance = null;
    
    ArrayBlockingQueue queue_reference= null;  
    ConcurrentLinkedQueue bridge_reference= null;
    public ImageFetcher(ArrayBlockingQueue queue_reference,ConcurrentLinkedQueue bridge_reference)
    {
        this.bridge_reference=bridge_reference;
        this.queue_reference= queue_reference;
    }
    
        int getImage(String imageUrl) throws IOException {
        url_instance= new URL(imageUrl);
        instance = (HttpURLConnection) url_instance.openConnection();
        instance.setRequestProperty("User-Agent","User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1");
        instance.setRequestMethod("GET");
        instance.setDoInput(true);
        instance.connect();
       if(instance.getResponseCode()== 200)
       {
           System.out.println(instance.getContentLength());
       }
       else
       {
           image_node.putResult(new ImageNode((Element)image_node.getData(),null));
           instance.disconnect();
           return 0;
       }
                BufferedImage image = ImageIO.read(instance.getInputStream());
                image_node.putResult(new ImageNode((Element)image_node.getData(),image));
                instance.disconnect();
                return 1;
	}
    
    
    @Override
    public void run()
    {
        image_node=(RSSNode)bridge_reference.poll();
        if(image_node==null)
        {
            try {
                image_node.putResult(new ImageNode((Element)image_node.getData(),null)); // Puttting result to result queue
                queue_reference.put(this);
            } catch (InterruptedException ex) {
                Logger.getLogger(ImageFetcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            image_node.getLatch().countDown();
            return ;
        }
        Element image = (Element)image_node.getData();
        System.out.println(image.toString());
        String image_url =image.attr("src");
        image_url = image_url.replaceAll(" ", "%20");    
        try {
            getImage(image_url);
        } catch (IOException ex) {
            Logger.getLogger(ImageFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            // Putting themself back to queue
            queue_reference.put(this);
            image_node.getLatch().countDown();
        } catch (InterruptedException ex) {
            Logger.getLogger(ImageFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
